var gulp = require('gulp');

var path = {
    build: {
        html:   'build/',
        js:     'build/js/',
        css:    'build/css/',
        img:    'build/img/',
        fonts:  'build/fonts/'
    },
    src: {
        html:   'src/*.html',
        js:     'src/js/main.js',
        style:  'src/style/main.less',
        img:    'src/img/**/*.*',
        fonts:  'src/fonts/**/*.*'
    },
    watch: {
        html:   'src/**/*.html',
        js:     'src/js/**/*.js',
        style:  'src/style/**/*.less',
        img:    'src/img/**/*.*',
        fonts:  'src/fonts/**/*.*'
    },
    clean:      './build'
};